const daftarMenu = () => {
    fetch("api/getMenu")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            let menuResto = "";
            data.map((menu) => {
                menuResto += `<div class="box-makanan">
            <img src="${menu.gambarMakanan}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${menu.namaMakanan}</h5>
                <p class="card-text" font-size="x-small">${menu.deskripsiMakanan}</p>
                <button class="btn btn-warning">Order</button>
            </div>
        </div>`;
            })

            const menuList = document.querySelector(".menu");
            menuList.innerHTML = menuResto;
        })
    };

daftarMenu();


//search

let jsondata = "";
let apiUrl = "api/getMenu"

async function getJson(url) {
    let response = await fetch(url);
    let data = await response.json()
    return data;
}

async function allData() {

    jsondata = await getJson(apiUrl)
    var daftarMenu = (menu) => {
        const daftarMenu = document.querySelector(".menu");

        daftarMenu.innerHTML += `<div class="box-makanan">
        <img src="${menu.gambarMakanan}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">${menu.namaMakanan}</h5>
            <p class="card-text" font-size="x-small">${menu.deskripsiMakanan}</p>
            <button class="btn btn-warning">Order</button>
        </div>
    </div>`;
    }
    jsondata.map(daftarMenu);

    //filter
     var searchData = document.querySelector(".cari-menu");
    searchData.addEventListener('keyup', () => {
        const search = jsondata.filter((menu) => {
            const input = document.querySelector(".cari-menu");
            const namaMenu = menu.namaMakanan.toLowerCase();
            const typeInput = input.value.toLowerCase();

            return namaMenu.includes(typeInput);
        })
        document.querySelector(".menu").innerHTML = ``;
        search.map(daftarMenu);
    });

    //reduce
    const jumlahPesanan = []
    const buttonPesan = document.querySelectorAll('.btn-warning');
    const elmntJumlahPesanan = document.querySelector('.jumlah-pesanan > p');

    buttonPesan.forEach((item, index) => {
        item.addEventListener('click', () => {
            jumlahPesanan.push(1);


            const hasil = jumlahPesanan.reduce((accumulator, currentValue) => {
                return accumulator + currentValue;
            }, 0);


            elmntJumlahPesanan.innerHTML = hasil;
        })
    })

}

allData();