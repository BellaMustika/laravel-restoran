const menu = [
	{
	gambarMakanan: "asset/img/loyang1.jpg", 
	namaMakanan: "Regular Pan",
	deskripsiMakanan: "Meat Lovers, Chicken Lovers, Super Supreme, Super Supreme Chicken, American Favourite,Pepperoni, Veggie Garden, Tuna Melt, Black Pepper Beef, Black Pepper Chicken, Cheese Lovers"
	}, {
	gambarMakanan: "asset/img/loyang2.jpg",
	namaMakanan: "Personal Pan",
	deskripsiMakanan: "Meat Lovers, Chicken Lovers, Super Supreme, Super Supreme Chicken, American Favourite,Pepperoni, Veggie Garden, Tuna Melt, Black Pepper Beef, Black Pepper Chicken, Cheese Lovers"
	}, {
	gambarMakanan: "asset/img/loyang3.jpg",
	namaMakanan: "Loversarge Pan",
	deskripsiMakanan: "Meat Lovers, Chicken Lovers, Super Supreme, Super Supreme Chicken, American Favourite,Pepperoni, Veggie Garden, Tuna Melt, Black Pepper Beef, Black Pepper Chicken, Cheese Lovers"
	}, {
	gambarMakanan: "asset/img/pasta1.jpg",
	namaMakanan: "Caneloni Chicken",
	deskripsiMakanan: "Extra Cheese Pasta"
	}, {
	gambarMakanan: "asset/img/pasta2.jpg",
	namaMakanan: "Beef Lasagna",
	deskripsiMakanan: "Extra Cheese Pasta"
	}, {
	gambarMakanan: "asset/img/pasta3.jpg",
	namaMakanan: "Creamy Beef Classic Fettuccine",
	deskripsiMakanan: "Extra Cheese Pasta"
	}, {
	gambarMakanan: "asset/img/soup.jpg",
	namaMakanan: "Salad Bar",
	deskripsiMakanan: "Tomat, Seledri, Kentang, Jagung, Telur Rebus, Makaroni, Paprika, Wortel, Timun, Bawang Bombay"
	}, {
	gambarMakanan: "asset/img/soup1.jpg",
	namaMakanan: "Puff Pastry Mushroom",
	deskripsiMakanan: "Soup Cream, Jagung, Kentang, Roti, Ayam"
	}, {
	gambarMakanan: "asset/img/soup2.jpg",
	namaMakanan: "Soup of Day",
	deskripsiMakanan: "Soup Cream, Jagung, Ayam"
	}, {
	gambarMakanan: "asset/img/paket2.jpg",
	namaMakanan: "Triple Box",
	deskripsiMakanan: "Regular Pizza ( All Topping ), Rocky Road Pizza, Rice Chicken Delight, Beef Lasagna, Chicken Popcorn, Sausage Frankfurter BBQ, Chicken Bruschetta, Garlic Bread"
	}, {
	gambarMakanan: "asset/img/paket1.png",
	namaMakanan: "Big Box",
	deskripsiMakanan: "Large PAN Pizza ( All Topping ), Beef Spaghetti, Mac 'N Cheese, Garlic Bread, Potato Wedges, Sausage Pastry Rolls, Choco Puff"
	}, {
	gambarMakanan: "asset/img/paket3.jpg",
	namaMakanan: "Double Box Regular",
	deskripsiMakanan: "2 Pizza All Topping & Crust Note Pilih 2 Pizza Apa Saja"
	}
];		

const callbackMap = (data, index)=>{
	const elmnt = document.querySelector('.menu');

	elmnt.innerHTML += `
			<div class="box-makanan">
			<img src="${data.gambarMakanan}" class="card-img-top" alt="...">
			<div class="card-body">
			  	<h5 class="card-title">${data.namaMakanan}</h5>
			    <p class="card-text" font-size="x-small">${data.deskripsiMakanan}</p>
			    <button class="btn btn-warning">Order</button>
			</div>
		</div>`
};

menu.map(callbackMap);

const buttonElmnt = document.querySelector('.button-cari');
buttonElmnt.addEventListener('click',()=>{
	const hasilPencarian = 
		menu.filter((data, index)=>{
			const inputElmnt = document.querySelector('.form-control');
			const namaMakananData = data.namaMakanan.toLowerCase();
			const keyword = inputElmnt.value.toLowerCase();

			return namaMakananData.includes(keyword);
	})

		document.querySelector('.menu').innerHTML = '';

	hasilPencarian.map(callbackMap);
});


//Reduce//
const jumlahPesanan = []
const buttonPesan = document.querySelectorAll('.btn-warning');
const elmntJumlahPesanan = document.querySelector('.jumlah-pesanan > p');

buttonPesan.forEach((item, index)=>{
item.addEventListener('click', ()=>{
	jumlahPesanan.push(1);
	
	
	const hasil = jumlahPesanan.reduce((accumulator, currentValue)=>{
		return accumulator + currentValue;
		}, 0);


	elmntJumlahPesanan.innerHTML = hasil;
	})
})