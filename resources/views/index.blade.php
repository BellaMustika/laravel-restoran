<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font Awesome -->
     <link rel="stylesheet"  href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>P I Z Z A P L A N E T</title>

    <!-- CSS -->
    <link href="{{ asset('/asset/css/style.css') }}" rel="stylesheet"> 

  </head>

  <body>
    <div class="menu-resto">
    <div class="container">

      <!-- Navbar -->
      @include('layouts.header')
  
      <!-- Carousel -->
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>

        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="{{ asset('/asset/img/pizza1.jpg') }}" class="d-block w-100" alt="..." height="555">
          </div>
          <div class="carousel-item">
            <img src="{{ asset('/asset/img/pizza2.jpg') }}" class="d-block w-100" alt="..." height="555">
          </div>
          <div class="carousel-item">
            <img src="{{ asset('/asset/img/pizza3.jpg') }}" class="d-block w-100" alt="..." height="555">
          </div>
          <div class="carousel-item">
            <img src="{{ asset('/asset/img/pizza4.jpg') }}" class="d-block w-100" alt="..." height="555">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

      <!-- Menu -->
      <br>
        <h3 id="Menu" style="text-align:center;">
          <a href="/Home">
            <img src= "{{ asset('/asset/img/logo.jpg') }}" width="50" height="40"></a>
            MENU
        </h3>
      <br>

      <!-- Box Makanan -->
      <div class="menu" style="display: flex; flex-wrap: wrap; justify-content: center;"></div>

      <!-- Reduce -->
      <div class="box-pesanan" id="Cart">
          <div class="jumlah-pesanan">
            <h2>YOUR ORDERS</h2>
            <p>0</p>
          </div>
      </div>

    <!-- Footer -->
    @include('layouts.footer')
        

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <!-- <script src="{{ asset('asset/js/Menu.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('asset/js/data.js') }}"></script>
  </body>
</html>
