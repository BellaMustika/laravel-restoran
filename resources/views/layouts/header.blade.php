<!-- Navbar -->
<nav class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="/Home">
    <img src= "{{ asset('/asset/img/logo.jpg') }}" width="50" height="40">
    <img src="{{ asset('/asset/img/logo1.png') }}" width= "170" height="30"></a>
        
<!--Button Navbar -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fa fa-bars"></i>
  </button>

<div class="collapse navbar-collapse" id="navbarNav">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link active" href="/Home">Home</a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="#Menu">Menu</a>
    </li>
  </ul>

<!-- Search --> 
<div class="form-inline my-2 my-log-0 ml-auto">  
  <div class="button-cari">
    <input class="form-control mr-sm-2 cari-menu" type="search" placeholder="Search" aria-label="Search">
  </div>

<div class="admin">
<!-- Admin -->
<a class="inline-block no-underline hover:text-black" href="#">
  <svg class="fill-current hover:text-black" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
  <circle fill="none" cx="14" cy="9" r="5"></circle>
    <path d="M12 2C9.243 2 7 4.243 7 7s2.243 5 5 5 5-2.243 5-5S14.757 2 12 2zM12 10c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3S13.654 10 12 10zM21 21v-1c0-3.859-3.141-7-7-7h-4c-3.86 0-7 3.141-7 7v1h2v-1c0-2.757 2.243-5 5-5h4c2.757 0 5 2.243 5 5v1H21z"></path>
    </svg>
</a>
            
<!-- Cart -->
<a class="pl-3 inline-block no-underline hover:text-black" href="#Cart">
  <svg class="fill-current hover:text-black" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">  
    <path d="M21,7H7.462L5.91,3.586C5.748,3.229,5.392,3,5,3H2v2h2.356L9.09,15.414C9.252,15.771,9.608,16,10,16h8 c0.4,0,0.762-0.238,0.919-0.606l3-7c0.133-0.309,0.101-0.663-0.084-0.944C21.649,7.169,21.336,7,21,7z M17.341,14h-6.697L8.371,9 h11.112L17.341,14z"></path>
    <circle cx="10.5" cy="18.5" r="1.5"></circle>     
    <circle cx="17.5" cy="18.5" r="1.5"></circle>
  </svg>
</a>
</div>
</div>
</div>
</nav>
</div>
</div>